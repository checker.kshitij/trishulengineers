<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Trishul Engineers</title>

    <!-- Favicon -->
    <!--<link rel="icon" href="images/favicon.png" type="image/x-icon" />-->
    <!-- Bootstrap CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- Animate CSS -->
    <link href="vendors/animate/animate.css" rel="stylesheet">
    <!-- Icon CSS-->
	<link rel="stylesheet" href="vendors/font-awesome/css/font-awesome.min.css">
    <!-- Camera Slider -->
    <link rel="stylesheet" href="vendors/camera-slider/camera.css">
    <!-- Owlcarousel CSS-->
	<link rel="stylesheet" type="text/css" href="vendors/owl_carousel/owl.carousel.css" media="all">

    <!--Theme Styles CSS-->
	<link rel="stylesheet" type="text/css" href="css/style.css" media="all" />

    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="js/html5shiv.min.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->

<link rel="stylesheet" type="text/css" href="css/fonts.css">
<link rel="stylesheet" type="text/css" href="css/font-awesome.css">
<!-- <link rel="stylesheet" href="css/layout.css" type="text/css"> -->
<link rel="stylesheet" href="css/animate.min.css">

<script src="js/jquery.min.js"></script>
<script src="js/skrollr.js"></script>
<script src="js/modernizr.custo.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


        <link rel="stylesheet" type="text/css" href="css/style2.css" />
</head>
<body>
    <!-- Preloader -->
    <!-- <div class="preloader"></div> -->

	<!-- Top Header_Area -->
	<section class="top_header_area">
	    <div class="container">
            <ul class="nav navbar-nav top_nav">
                <li><a href="tel:+919823114932"><i class="fa fa-phone"></i>+91 9823114932</a></li>
                <li><a href="mailto:info@trishulengineers.in"><i class="fa fa-envelope-o"></i>info@trishulengineers.in</a></li>
                <li><a href="#"><i class="fa fa-map-marker"></i> J-312, MIDC, Bhosari, Pune, Maharashtra 411026</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right social_nav">
                <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                <!--<li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                <li><a href="#"><i class="fa fa-pinterest-p" aria-hidden="true"></i></a></li>-->
                <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
            </ul>
	    </div>
	</section>
	<!-- End Top Header_Area -->

	<!-- Header_Area -->
    <nav class="navbar navbar-default header_aera" id="main_navbar">
        <div class="container">
            <!-- searchForm -->
            <!--<div class="searchForm">
                <form action="#" class="row m0">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-search"></i></span>
                        <input type="search" name="search" class="form-control" placeholder="Type & Hit Enter">
                        <span class="input-group-addon form_hide"><i class="fa fa-times"></i></span>
                    </div>
                </form>
            </div>--><!-- End searchForm -->
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="col-md-2 p0">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#min_navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.html"><img src="images/logo.png" alt=""></a>
                </div>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="col-md-8 p0">
                <div class="collapse navbar-collapse" id="min_navbar">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="index.html">Home</a></li>
                        <li><a href="about-us.html">About Us</a></li>
                        <li class="dropdown submenu">
                            <a href="products.html" class="dropdown-toggle" >Products</a>
                            <ul class="dropdown-menu other_dropdwn">
                                <li><a href="acoustic-jackets.html">Acoustic Jackets</a></li>
                                <li><a href="acoustical-test-and-measurement-chambers.html">Acoustical Test & Measurement Chambers</a></li>
                                <li><a href="audiology-chambers-and-rooms.html">Audiology Chambers and Rooms</a></li>
                                <li><a href="engine-exhaust-silencers.html">Engine Exhaust Silencers</a></li>
                                <li><a href="modular-acoustic-panels.html">Modular Acoustic Panels</a></li>
                                <li><a href="acoustic-doors.html">Acoustic Doors</a></li>
                                <li><a href="quadri-tone-acoustic-panels.html">Quadri-tone Acoustic Panels</a></li>
                                <li><a href="tri-tone-acoustic-panels.html">Tri-tone Acoustic Panels</a></li>  
                                <li style="display:none;"><a href="acoustic-fan-jackets.html">Acoustic Fan Jackets</a></li>
                                <li style="display:none;"><a href="acoustic-insulation-jacket.html">Acoustic Insulation Jacket</a></li>
                                <li style="display:none;"><a href="industrial-sound-jackets.html">Industrial Sound Jackets</a></li>
                                <li style="display:none;"><a href="noise-control-systems.html">Noise Control Systems</a></li>
                                <li style="display:none;"><a href="ancloz-acoustic-jackets.html">Ancloz Acoustic Jackets</a></li>
                                <li style="display:none;"><a href="acoustic-chambers.html">Acoustic Chambers</a></li>
                                <li style="display:none;"><a href="dg-exhaust-silencers.html">DG Exhaust Silencers</a></li>
                                <li style="display:none;"><a href="sound-proof-cabin.html">Sound Proof Cabin</a></li>
                                <li style="display:none;"><a href="industrial-sound-proof-cabins.html">Industrial Sound Proof Cabins</a></li>
                            </ul>
                        </li>
                        <li><a href="gallery.html">Gallery</a></li>
                        <li><a href="contact-us.html">Contact Us</a></li>
                        <li><a href="enquiry.php">Enquiry</a></li>
                        <!--<li><a href="#" class="nav_searchFrom"><i class="fa fa-search"></i></a></li>-->
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div>
            <div class="col-md-2">
                <div class="navbar-header">
                    <a class="navbar-brand" href="index.html"><img src="images/awards.jpg" width="70" alt=""></a>
                </div>
            </div>
        </div><!-- /.container -->
    </nav>
	<!-- End Header_Area -->

    <!-- Banner area -->

   <div id="layout">
        
        <div id="banner">
                       <div id="da-slider" class="da-slider">
                <div class="da-slide">
                    <h2>Engine Exhaust Silencers</h2>
                    <p>In noise control, the need for an acoustic filter or silencer is encountered when it becomes necessary to transport air or gases from one place to another through a duct or tube.</p>
                    <a href="engine-exhaust-silencers.html" class="da-link">Read more</a>
                    <div class="da-img"><img src="images/banner/1.png" alt="" width="500" /></div>
                </div>
                <div class="da-slide">
                    <h2>Modular Acoustic Panels</h2>
                    <p>Trishul acoustic panels consists of pre-engineered folded sheet metal panels that can be configured and assembled into a wide variety of structures used to control and reduce excessive noise levels in industrial, commercial, institutional and community environments.</p>
                    <a href="modular-acoustic-panels.html" class="da-link">Read more</a>
                    <div class="da-img"><img src="images/banner/2.png" alt="" width="350" /></div>
                </div>
                <div class="da-slide">
                    <h2>Audiology Chambers and Rooms</h2>
                    <p>Trishul offers standard screening chambers and customized audiology rooms / suite. Our chambers and suites are made of all steel construction with acoustic insulation which provides optimum sound isolation and absorption. Our chambers and suites come in variety of colours, trims and finishes to match any interior decor.</p>
                    <a href="audiology-chambers-and-rooms.html" class="da-link">Read more</a>
                    <div class="da-img"><img src="images/banner/3.png" alt="" width="400" /></div>
                </div>
                <div class="da-slide">
                    <h2>Acoustical Test and <br>Measurement Chambers</h2>
                    <p>Trishul can design and manufacture engineered acoustical test chambers that will either isolate the test chamber from the ambient providing a specific, controlled interior acoustical environment</p>
                    <a href="acoustical-test-and-measurement-chambers.html" class="da-link">Read more</a>
                    <div class="da-img"><img src="images/banner/4.png" alt="" width="280" /></div>
                </div>
                <div class="da-slide">
                    <h2>Acoustic Jackets</h2>
                    <p>When noisy equipment is a problem, removable acoustic insulation jackets may be the solution. We engineer our acoustic jackets to combat your specific needs with the perfect combination of absorbing, damping and reflective materials.</p>
                    <a href="acoustic-jackets.html" class="da-link">Read more</a>
                    <div class="da-img"><img src="images/banner/5.png" alt="" width="250"/></div>
                </div>

                <div class="da-slide">
                    <h2>Quadri-tone-acoustic</h2>
                    <p>Trishul acoustic panels consists of pre-engineered folded sheet metal panels that can be configured and assembled into a wide variety of structures used to control and reduce excessive noise levels in industrial, commercial, institutional and community environments.</p>
                    <a href="modular-acoustic-panels.html" class="da-link">Read more</a>
                    <div class="da-img"><img src="images/banner/6.png" alt="" width="350"/></div>
                </div>

               

                <div class="da-slide">
                    <h2>Acoustic Doors</h2>
                    <p>To provide solutions for the widest variety of applications, our product features an array of integrated, related
                    components providing high performance acoustical characteristics combined with rugged structural integrity and  design flexibility.</p>
                    <a href="acoustic-doors.html" class="da-link">Read more</a>
                    <div class="da-img"><img src="images/banner/7.png" alt="" width="380"/></div>
                </div>

                 <div class="da-slide">
                    <h2>Tri-tone Acoustic Panels</h2>
                    <p>To provide solutions for the widest variety of applications, our product features an array of integrated, related
                    components providing high performance acoustical characteristics combined with rugged structural integrity and  design flexibility.</p>
                    <a href="acoustic-doors.html" class="da-link">Read more</a>
                    <div class="da-img"><img src="images/banner/8.png" alt="" width="380"/></div>
                </div>
                <nav class="da-arrows">
                    <span class="da-arrows-prev"></span>
                    <span class="da-arrows-next"></span>
                </nav>
            </div>
        </div>         
</div>

<style type="text/css">
    button, input, select, textarea {
    font-family: inherit;
    font-size: inherit;
    line-height: inherit;
    margin-top: 10px;
}
</style>
    <section class="about_us_area row">
        <div class="container">

            <div class="tittle wow fadeInUp">
                <h2>ENQUIRY</h2>
                
            </div>
            <div class="col-md-2"></div>
            <div class="col-md-8">
             <form name="myform" method="post" action="contact-pro.php">
<table width="643" border="0" cellpadding="5" cellspacing="3">
    <tr>
    <td width="261" class="hostfea"><strong><font color="#000000">Name</font></strong></td>
    <td width="353"> 
      <input name="name" type="text" size="50" /> </td>
  </tr>
  <tr>
    <td class="hostfea"><strong><font color="#000000">Company Name </font></strong></td>
    <td> 
      <input name="cname" type="text" size="50" /> </td>
  </tr>
    <tr>
    <td class="hostfea"><strong><font color="#000000">Email</font></strong></td>
    <td> 
      <input name="email" type="text" size="50" /> </td>
  </tr>
  
  <tr>
    <td class="hostfea"><strong><font color="#000000">Phone</font></strong></td>
    <td> 
      <input name="phone" type="text" size="50" /> </td>
  </tr>
  
   <tr>
    <td class="hostfea"><strong><font color="#000000">Product Name</font></strong></td>
    <td> 
      <input name="pname" type="text" size="50" /> </td>
  </tr>
  
  
 <tr>
    <td class="hostfea"><strong><font color="#000000">Message</font></strong></td>
    <td> 
      <input type="text" name="msg" size="50" /> </td>
  </tr>
  
  <tr>
    <td class="hostfea">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
 
  
  <tr>
        <td class="td"><strong><font color="#000000">Security code</font></strong> </td>
        <td class="td">
        <img src="captcha.php" /> <br/><br/>
        <input type="text" class="input"  name="cd"> <br/> Enter code from above image.
        </td>
    </tr>
  <tr>
    <td colspan="4" align="center"  style="padding-left:100px;">
    <input type="submit" name="Submit" value="Submit" />&nbsp;&nbsp;&nbsp;  </tr>
  <tr><td>
  
</td></tr>
</table>
</form>
            </div>
        </div>
    </section>
    <!-- End About Us Area -->

    <!-- Footer Area -->
    <br><br>
    <footer class="footer_area">
        <div class="container">
            <div class="footer_row row">
                <div class="col-md-3 col-sm-6 footer_about">
                    <h2>ABOUT OUR COMPANY</h2>
                    <!--<img src="images/footer-logo.png" alt="">-->
                    <p>Our company, established in the year 1989, has been dedicated to the field of noise control and measurement products.</p>
                    <ul class="socail_icon">
                        <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                    </ul>
                </div>
                <div class="col-md-2 col-sm-6 footer_about quick">
                    <h2>Quick links</h2>
                    <ul class="quick_link">
                        <li><a href="index.html"><i class="fa fa-chevron-right"></i>Home</a></li>
                        <li><a href="about-us.html"><i class="fa fa-chevron-right"></i>About Us</a></li>
                        <li><a href="products.html"><i class="fa fa-chevron-right"></i>Products</a></li>
                        <li><a href="gallery.html"><i class="fa fa-chevron-right"></i>Gallery</a></li>
                        <li><a href="contact-us.html"><i class="fa fa-chevron-right"></i>Contact Us</a></li>
                        <li><a href="enquiry.php"><i class="fa fa-chevron-right"></i>Enquiry</a></li>
                    </ul>
                </div>
                <div class="col-md-4 col-sm-6 footer_about">
                    <h2>CONTACT US</h2>
                    <address>
                        <p>Have questions, comments or just want to say hello:</p>
                        <ul class="my_address">
                            <li><a href="mailto:info@trishulengineers.in"><i class="fa fa-envelope" aria-hidden="true"></i>info@trishulengineers.in</a></li>
                            <li><a href="tel:+919823114932"><i class="fa fa-phone" aria-hidden="true"></i>+91 9823114932</a></li>
                            <li><a href="tel:02046768207"><i class="fa fa-phone" aria-hidden="true"></i>(020) 46768207</a></li>
                            <li><a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i><span>J-312, MIDC, Bhosari, Pune, Maharashtra 411026 <br><br></span></a></li>
                        </ul>
                    </address>
                </div>
                <div class="col-md-3 col-sm-6 footer_about">
                    <h2>LOCATION MAP</h2>
                  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1890.3284781175444!2d73.82884515794382!3d18.634493196835706!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bc2c78c00000001%3A0x581fb5088ed07799!2sTrishul+Engineers!5e0!3m2!1sen!2sin!4v1536037657185" width="100%" height="200" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
            </div>
        </div>
        <div class="copyright_area">
            Copyright 2018 All rights reserved. Design & Developed by <a href="http://weblinkservices.net/" target="_blank" style="color: #fff;">Web Link Services Pvt.Ltd.</a></a>
        </div>
    </footer>
    <!-- End Footer Area -->

    <!-- jQuery JS -->
    <script src="js/jquery-1.12.0.min.js"></script>
    <!-- Bootstrap JS -->
    <script src="js/bootstrap.min.js"></script>
    <!-- Animate JS -->
    <script src="vendors/animate/wow.min.js"></script>
    <!-- Camera Slider -->
    <script src="vendors/camera-slider/jquery.easing.1.3.js"></script>
    <script src="vendors/camera-slider/camera.min.js"></script>
    <!-- Isotope JS -->
    <script src="vendors/isotope/imagesloaded.pkgd.min.js"></script>
    <script src="vendors/isotope/isotope.pkgd.min.js"></script>
    <!-- Progress JS -->
    <script src="vendors/Counter-Up/jquery.counterup.min.js"></script>
    <script src="vendors/Counter-Up/waypoints.min.js"></script>
    <!-- Owlcarousel JS -->
    <script src="vendors/owl_carousel/owl.carousel.min.js"></script>
    <!-- Stellar JS -->
    <script src="vendors/stellar/jquery.stellar.js"></script>
    <!-- Theme JS -->
    <script src="js/theme.js"></script>
    <script type="text/javascript" src="js/jquery.cslider.js"></script>
        <script type="text/javascript">
            $(function() {
            
                $('#da-slider').cslider({
                    autoplay    : true,
                    bgincrement : 450
                });
            
            });
        </script>
</body>
</html>
