<?php

    if(isset($_POST['email'])) {
        // EDIT THE 2 LINES BELOW AS REQUIRED
     
        function died($error) {
            // your error code can go here
            echo "We are very sorry, but there were error(s) found with the form you submitted. ";
            echo "These errors appear below.<br /><br />";
            echo $error."<br /><br />";
            echo "Please go back and fix these errors.<br /><br />";
            die();
        }
        // validation expected data exists
        if(!isset($_POST['name']) ||
           !isset($_POST['cname']) ||
           !isset($_POST['phone']) ||
           !isset($_POST['email']) ||
		   !isset($_POST['pname']) ||
           !isset($_POST['msg'])  ||
		   !isset($_POST['cd']))
		   {
            died('We are sorry, but there appears to be a problem with the form you submitted.');       
        }
		
        $name = $_POST['name']; // required
        $c_name= $_POST['cname']; // required
        $phone = $_POST['phone']; // required
        $email = $_POST['email']; // not required
        $message = $_POST['msg']; // required
		$productname = $_POST['pname']; // required
		$captcha = $_POST['cd']; // required
        $error_message = "";
        $email_exp = '/^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/';
        if(!preg_match($email_exp,$email)) {
            $error_message .= 'The Email Address you entered does not appear to be valid.<br />';
        }
        $string_exp = "/^[A-Za-z .'-]+$/";
        if(!preg_match($string_exp,$name)) {
            $error_message .= 'The  Name you entered does not appear to be valid.<br />';
        }
        if(!preg_match($string_exp,$c_name)) {
            $error_message .= 'The company  Name you entered does not appear to be valid.<br />';
        }
        if(strlen($message) < 2) {
            $error_message .= 'The message you entered do not appear to be valid.<br />';
        }
        if(strlen($error_message) > 0) {
            died($error_message);
        }
		if($_POST['cd']!=$captcha)
	{
	
		$error_message .= 'Please Enter Valid Captcha Code.';
		
	}
        $email_message = "Form details below.\n\n";
        function clean_string($string) {
            $bad = array("content-type","bcc:","to:","cc:","href");
            return str_replace($bad,"",$string);
        }
	
	
        $email_message .= " Name: ".clean_string($name)."\n";
        $email_message .= "Company Name: ".clean_string($c_name)."\n";
        $email_message .= "Email: ".clean_string($phone)."\n";
		$email_message .= "Product Name: ".clean_string($email)."\n";
		$email_message .= "Phone: ".clean_string($productname)."\n";
        $email_message .= "Message: ".clean_string($message)."\n";
        // create email headers
		
		   $email_to = "info@trishulengineers.in";
        $email_subject = "Weblink enquiry mail";
		
      $headers = 'From: '.$email."\r\n".
    'Bcc: sales@weblinkservices.net' . "\r\n" .
    'X-Mailer: PHP/' . phpversion();
        @mail($email_to, $email_subject, $email_message, $headers);  
		   echo "<script type='text/javascript'>alert('Your email was sent!')</script>";
echo "<script>document.location='enquiry.php'</script>";

		   }
        ?>
